(function ($) {
    "use strict";

    // header sticky
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 768) {
            $(".header-area").removeClass("sticky");
        } else {
            $(".header-area").addClass("sticky");
        }
    });

    // scrollIt (for header)
    $.scrollIt({
        upKey: 0, // key code to navigate to the next section
        downKey: 0, // key code to navigate to the previous section
        easing: 'linear', // the easing function for animation
        scrollTime: 900, // how long (in ms) the animation takes
        activeClass: 'active', // class given to the active nav element
        onPageChange: null, // function(pageIndex) that is called when page is changed
        topOffset: -35 // offste (in px) for fixed top navigation
    });

    // meanmenu
    $('#menu').meanmenu({
        meanMenuContainer: '.mean-menu-wrap',
        meanScreenWidth: "991",
        onePage: true,
    });

    // data-background (for background image)
    $("[data-background]").each(function () {
        $(this).css("background-image", "url(" + $(this).attr("data-background") + ")")
    });
    
    // testimonial-carousel
    $('.testimonial-carousel').owlCarousel({
        loop: true,
        margin: 0,
        items: 1,
        autoplay: false,
        autoplayTimeout: 5000,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        nav: false,
        dots: true,
    });
    
    // partner-carousel
    $('.partner-carousel').owlCarousel({
        loop: true,
        margin: 0,
        items: 4,
        autoplay: false,
        autoplayTimeout: 5000,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        nav: false,
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            480: {
                items: 1,
            },
            768: {
                items: 2,
                nav: false,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 4,
            }
        }
    });

    // isotop massonry
    $('.grid').imagesLoaded(function () {
        // init Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
                // use outer width of grid-sizer for columnWidth
                columnWidth: 1,
            }
        });
        // filter items on button click
        $('.button-group').on('click', 'button', function () {
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({
                filter: filterValue
            });
        });
    });
    //for isotop massonry menu active class
    $('.button-group > button').on('click', function (event) {
        $(this).siblings('.active').removeClass('active');
        $(this).addClass('active');
        event.preventDefault();
    });

    // WOW active
    new WOW().init();
     
    $(window).load(function() {
        // body-preloader
        $(".body-loader-wrap").fadeOut();
    }); // winddow load function end
    
})(jQuery);